Luxinten base module for technical tasks

Tasks list:


1. Please, create table with 5 fields: 
    Article ID | Url | Article Name | Article Description | Language 
    ( the DB table can be extended according to your recommendation )

2. Please, create functionality which will add prefix: "Test - " to each page title,
    the final page title must be looks like: "Test - About us"

3. Please, add functionality which will check product URL before save,
    in case the same URL already exists will be add: "-1" to new URL

4. Please, create new template, insert it in footer of each page and show there some text from:
    Stores > Settings > Configuration > General > Luxinten Test > Message
    ( The showing text must be security protected )

5. Please, create custom logger which will save logs into file:
    var/log/test/unit.log


Additional information:

1. For start you must clone the module into your project with Magento 2.x

2. Create new branch: UNIT_Testing

3. Each finished point must be committed separately

4. Points 1-4 are required, point 5 is additional point.
